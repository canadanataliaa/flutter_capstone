import 'package:flutter/material.dart';

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.red[700], // background color
    side: BorderSide(width: 1.0, color: Colors.red),
    onPrimary: Colors.white // text color
);

var btnWhiteDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white, // background color
    side: BorderSide(width: 1.0, color: Colors.black),
    onPrimary: Colors.black // text color
);