import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/providers/user_provider.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;

        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),   
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: SingleChildScrollView(
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                      SizedBox(height: 36.0),
                                      Image.asset(
                                          'assets/ffuf-logo.png',
                                          color: Colors.white,
                                          width: 165.0,
                                      ), // Modify width and color of Image widget to match given sample.
                                      Container(
                                          margin: EdgeInsets.only(top: 16.0),
                                          child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                  Text('FFUF Project Management', style: TextStyle(fontSize: 22.0, color: Colors.white)),
                                                  Text('Made by FFUF Internal Dev Team', style: TextStyle(fontSize: 15.5, color: Colors.white)),
                                              ]
                                          )
                                      )
                                  ]
                              ),
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                ListTile(
                                    leading: Icon(Icons.person_pin_rounded, color: Colors.white),
                                    horizontalTitleGap: 2.0,
                                    title: Text('Get to know me!', style: TextStyle(color: Colors.white, fontSize: 16.0)),
                                    onTap: () {
                                        Navigator.pushNamed(context, '/get-to-know');
                                    },
                                ),
                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white, fontSize: 16.0)),
                                    onTap: () async {
                                        setAccessToken(null);
                                        setDesignation(null);

                                        SharedPreferences prefs = await SharedPreferences.getInstance();

                                        prefs.remove('accessToken');
                                        prefs.remove('designation');

                                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                                    }
                                ),
                            ]
                        )
                    ]
                )
            )
        );
    }
}