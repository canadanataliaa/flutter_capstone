import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';
import '/utils/api.dart';
import '/utils/functions.dart';
import '/utils/themes.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}
class _TaskCard extends State<TaskCard> {    

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        // Modify both the main and cross axis alignment.
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                            Text(
                                widget._task.title,
                                style: TextStyle(
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.w400
                                ),
                            ),
                            Text(
                                widget._task.description,
                                style: TextStyle(
                                    fontSize: 15.5
                                ),
                            )
                        ]
                    )
                ),
                Chip(label: Text(widget._task.status!))
            ]
        );

        void _updateTaskStatus(String status) {
            API(accessToken).updateTaskStatus(
                taskId: widget._task.id, 
                status: status
            ).catchError((error) {
                showSnackBar(context, error.message);
            });

            widget._reloadTasks();
        }

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Start'),
                onPressed: () => _updateTaskStatus('Ongoing')
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Finish'),
                onPressed: () => _updateTaskStatus('Completed')
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Accept'),
                onPressed: () => _updateTaskStatus('Accepted')
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Reject'),
                style: btnWhiteDefaultTheme,
                onPressed: () => _updateTaskStatus('Rejected')
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text(
                    'Detail',
                    style: TextStyle(
                        fontSize: 16.5,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                    )
                ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        List<Widget> btnStatus(String? designation, String? status) {
            List<Widget> buttons = [];
            if (designation == 'assembly-team' && status == 'Pending') {
                buttons.add(btnStart);
            } else if (designation == 'assembly-team' && status == 'Ongoing') {
                buttons.add(btnFinish);
            } else if (designation == 'contractor' && status == 'Rejected') {
                buttons.add(btnAccept);
            } else if (designation == 'contractor' && status == 'Accepted') {
                buttons.add(btnReject);
            } else if (designation == 'contractor' && status == 'Completed') {
                buttons.addAll([btnAccept, btnReject]);
            } else {
                buttons.add(Container());
            }
            
            return buttons;
        }

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),
                ...btnStatus(designation, widget._task.status)
                // Show btnStart if designation is assembly team and task status is pending, else show an empty container.
                // Show btnFinish if designation is assembly team and task status is ongoing, else show an empty container.
                // Show btnAccept if designation is contractor and task status is completed or rejected, else show an empty container.
                // Show btnReject if designation is contractor and task status is completed or accepted, else show an empty container.
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}