# Project Management

The objective of this Flutter Capstone titled as Project Management is to apply the knowledge gained from learning Dart and Flutter. 

The following are the packages used with versions, the project features and a step-by-step instructions on how to clone the project locally.

# Setup Guide

1. Go to bitbucket repository [https://bitbucket.org/canadanataliaa/flutter_capstone/src/master/](https://bitbucket.org/canadanataliaa/flutter_capstone/src/master/)
2. On the upper right corner, click on the **Clone** button.

    ![Untitled](/readme_images/Untitled.png)

3. Copy the **`git clone <repository-link>`** command line when prompted. 

    ![Untitled](/readme_images/Untitled%201.png)

4. Navigate to your local machine's file explorer. 
5. Go to the directory you want to save your project and open a terminal.
6. Right-click on the terminal to paste the command line copied from **Step 3**.
7. Once done, open the project in your choice of Editor. For VS Code, enter **`code .`** on the terminal.
8. In VS Code, click **Get packages** on the lower left when prompted.

    ![Untitled](/readme_images/Untitled%202.png)

9. If there is no prompt, open a terminal and navigate inside the **`csp_flutter`** and **`csp_api`** folders, and enter the command: **`flutter pub get`**
10. After installing the packages, change directory to **`csp_api`** and enter **`dart run bin/main.dart`** to run the server.
11. Run the flutter app.
    - Using the command-line interface.
        - On the terminal, navigate to **`csp_flutter`** folder and enter **`flutter run lib/main.dart`**
    - Using the graphical user interface/shortcuts in VS Code.
        - Inside **`lib`** folder, open the **`main.dart`** file. Click on Run (**F5**) to run the app or Debug (**Ctrl+F5**) to start debugging mode.

# Packages

- Flutter SDK: >=2.12.0 <3.0.0
- [http: ^0.13.3](https://pub.dev/packages/http)
- [provider: ^5.0.0](https://pub.dev/packages/provider/versions/5.0.0-nullsafety.5)
- [shared_preferences: ^2.0.6](https://pub.dev/packages/shared_preferences)
- [image_picker: ^0.8.0+3](https://pub.dev/packages/image_picker/versions/0.8.0+3)
- [flutter_dotenv: ^5.0.0](https://pub.dev/packages/flutter_dotenv/versions/5.0.0)
- [google_fonts: ^2.1.0](https://pub.dev/packages/google_fonts)
- [simple_animations: ^1.3.3](https://pub.dev/packages/simple_animations/versions/1.3.3/install)

# Flutter Version

- [Flutter 2.2.3](https://flutter.dev/docs/development/tools/sdk/releases)

# Project Features

### Login Feature

- Saves token to app state
- Saves token to local storage

### Contractor

- Can see the list of projects
    - Each project on the list has an Assign and Tasks button
- Can add new projects
- Can assign a project to a subcontractor
- When a project task has been completed, a contractor can accept or reject the task

### Project Task List Feature

- Project task list with status and details is shown when the Tasks button is clicked

### Subcontractor

- Can see assigned projects and tasks assigned to it
- Can create new tasks
- Assigns a task to an assembly team upon task creation

### Assembly Team

- Can see the list of projects and assigned tasks to it
- Can tag a task as ongoing or completed

# Test User Credentials

Use the following credentials to test the application

| Email | Password |
| ------------- | ------------- |
| contractor@gmail.com | contractor |
| subcontractor@gmail.com | subcontractor |
| assembly-team@gmail.com | assembly-team |
